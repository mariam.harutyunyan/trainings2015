#include <iostream>

int
main()
{
    int a, b, c, min, max;
    std::cout << "Enter three numbers: ";
    std::cin >> a >> b >> c;
    std::cout << "sum = " << a + b + c << std::endl;
    std::cout << "average value = " << (a + b + c) / 3 << std::endl;
    std::cout << "product = " << a * b * c << std::endl;

    min = a;
    max = a;

    if(b < min){
        min = b;
    }
    if(c < min){
        min = c;
    }
    std::cout << "Minimal value = " << min << std::endl;

    if (b > max){
        max = b;
    }
    if (c > max){
        max = c;
    }
    std::cout << "Maximal value = " << max << std::endl;

    return 0;   
}
