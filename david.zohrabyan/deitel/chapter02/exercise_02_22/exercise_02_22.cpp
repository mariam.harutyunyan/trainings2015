///This program prints stars
#include <iostream>///standard input output

using std::cout;///the program uses cout

//function main begins program execution
int main()
{

 cout << "*\n**\n***\n****\n*****\n";///display the stars
  
 return 0;///program ended successfully
}///end function main
