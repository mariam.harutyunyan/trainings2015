/*The program defines, whether exceeded 
expenses of the client having the deposit account, a credit limit.*/
#include <iostream>
#include <iomanip>

int
main()
{
    int account;
    std::cout << "Enter an account number (-1 to quit): ";
    std::cin >> account;
    
    while(account != -1)
    {
        double openningBalance;
        std::cout << "Enter initial balance: " ;
        std::cin >> openningBalance;
        if(openningBalance < 0) {
            std::cout << "Entered balance " << openningBalance << " is invalid. It will set 0." << std::endl;
            openningBalance = 0;
        }
        
        double sumOfExpenses;
        std::cout << "Enter the sum of expenses: ";
        std::cin >> sumOfExpenses;
        if(sumOfExpenses < 0) {
            std::cout << "Entered sum of expenses " << sumOfExpenses << " is invalid. It will set 0." << std::endl;
            sumOfExpenses = 0;
        }
        
        double arrivalSum;
        std::cout << "Enter the arrival sum: ";
        std::cin >> arrivalSum;
        if(arrivalSum < 0) {
            std::cout << "Entered arrival sum " << arrivalSum << " is invalid. It will set 0." << std::endl;
            arrivalSum = 0;
        }
        
        double creditLimit;
        std::cout << "Enter a credit limit: ";
        std::cin >> creditLimit;
        if(creditLimit < 0) {
            std::cout << "Entered balance " << creditLimit << " is invalid. It will set 0." << std::endl;
            creditLimit = 0;
        }
        
        std::cout << "New Balance: " << (openningBalance + sumOfExpenses - arrivalSum) << std::endl;
        
        if(openningBalance + sumOfExpenses - arrivalSum > creditLimit) {
            std::cout << "Account: " << account << std::endl;
            std::cout << "Credit Limit: " << std::setprecision(2) << std::fixed << creditLimit << std::endl;
            std::cout << "Balance: " << (openningBalance + sumOfExpenses - arrivalSum) << std::endl;
            std::cout << "The limit of the credit is exceeded." << std::endl;
        }
        
        std::cout << "Enter an account number (-1 to quit): ";
        std::cin >> account;
    }
          
    return 0;
}
 
 
